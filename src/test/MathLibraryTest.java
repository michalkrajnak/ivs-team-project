/*******************************************************************
* Project name: IVS - Calculator
* File: MathLib.java
* Date: 25.4.2017
* Last modification: 26.4.2017
* Author: Dušan Valecký xvalec00(at)stud.fit.vutbr.cz
*
* Description: JUnit tests for functionality testing implemented mathematical methods.
*
*******************************************************************/
/**
* @file MathLibraryTest.java
*
* @brief JUnit tests for MathLib.java.
* @author Dušan Valecký
*/

package test;

import static org.junit.Assert.*;

import org.junit.Test;

import app.backend.exception.WrongExpressionException;
import app.math_lib.MathLib;

public class MathLibraryTest {
	/**
	 * The maximum delta between expected and actual values for which
	 * both numbers are still considered equal.
	 */
	private static final double DELTA = 1e-15;

	/**
	 * Testing MathLib.add method.
	 */
	@Test
	public void testAdd() {
		assertEquals(72.0+55.0, MathLib.add(72.0, 55.0), DELTA);
		assertEquals(4.7+2.8, MathLib.add(4.7, 2.8), DELTA);
		assertEquals(578.754+224.83, MathLib.add(578.754, 224.83), DELTA);
		assertEquals(458.7344+874.834, MathLib.add(458.7344, 874.834), DELTA);
	}

	/**
	 * Testing MathLib.sub method.
	 */
	@Test
	public void testSub() {
		assertEquals(72.0-44.0, MathLib.sub(72.0, 44.0), DELTA);
		assertEquals(7.6-5.4, MathLib.sub(7.6, 5.4), DELTA);		
		assertEquals(754.84-126.52, MathLib.sub(754.84, 126.52), DELTA);
		assertEquals(214.84-526.452, MathLib.sub(214.84, 526.452), DELTA);
	}
	
	/**
	 * Testing MathLib.div method.
	 */
	@Test
	public void testDiv() {
		assertEquals(15.0/5.0, MathLib.div(15.0, 5.0), DELTA);
		assertEquals(7.6/5.4, MathLib.div(7.6, 5.4), DELTA);
		assertEquals(-85.4/24.5, MathLib.div(-85.4, 24.5), DELTA);
		assertEquals(-125.4/284.5, MathLib.div(-125.4, 284.5), DELTA);
	}
	
	/**
	 * Testing MathLib.mul method.
	 */
	@Test
	public void testMul() {
		assertEquals(3.0*5.0, MathLib.mul(3.0, 5.0), DELTA);
		assertEquals(7.6*5.4, MathLib.mul(7.6, 5.4), DELTA);
		assertEquals(-742.83*120.45, MathLib.mul(-742.83, 120.45), DELTA);
	}
	
	/**
	 * Testing MathLib.factorial method.
	 */
	@Test
	public void testFactorial() {		
		assertEquals(6.0, MathLib.factorial(3.0), DELTA);
		assertEquals(720.0, MathLib.factorial(6.0), DELTA);
	}
	
	/**
	 * Testing MathLib.power method.
	 */
	@Test
	public void testPow() {		
		assertEquals(Math.pow(4.0, 3.0), MathLib.power(4.0, 3.0), DELTA);
		assertEquals(Math.pow(-14.0, 8.0), MathLib.power(-14.0, 8.0), DELTA);
		assertEquals(Math.pow(0.0, 8.0), MathLib.power(0.0, 8.0), DELTA);
		assertEquals(Math.pow(14.0, 0.0), MathLib.power(14.0, 0.0), DELTA);
		assertEquals(Math.pow(132.24, 11.0), MathLib.power(132.24, 11.0), DELTA);
		assertEquals(Math.pow(-71.0, 3.0), MathLib.power(-71.0, 3.0), DELTA);
	}
	
	/**
	 * Testing MathLib.root method.
	 */
	@Test
	public void testRoot() {		
		assertEquals(Math.pow(36.0, 1.0/2.0), MathLib.root(36.0, 2.0), DELTA);
		assertEquals(Math.pow(14.0, 1.0/8.0), MathLib.root(14.0, 8.0), DELTA);
		assertEquals(Math.pow(67.21, 1.0/8.0), MathLib.root(67.21, 8.0), DELTA);
	}
	
	/**
	 * Testing WrongExpressionException
	 */
	@Test(expected=WrongExpressionException.class)
	public void testWrongExpressionException() {
		MathLib.root(-67.21, 8.0);
		MathLib.div(67.21, 0.0);
		MathLib.factorial(-21.0);
		MathLib.power(-243.21, 41.0);
	}
	
	/**
	 * Testing MathLib.exp method.
	 */
	@Test
	public void testExp() {		
		assertEquals(Math.exp(3.0), MathLib.exp(3.0), DELTA);
		assertEquals(Math.exp(0.7), MathLib.exp(0.7), DELTA);
		assertEquals(Math.exp(27.7), MathLib.exp(27.7), DELTA);
	}
}
/*** End of file MathLibraryTest.java ***/
