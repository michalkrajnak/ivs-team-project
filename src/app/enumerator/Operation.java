package app.enumerator;

/**
 * Created by tomas on 4/6/17.
 */
public enum Operation {
    RESULT,
    ADD,
    MINUS,
    MUL,
    DIV,
    FAKT,
    SQRT,
    SQR,
    CUSTOM,
    DELETE,
    CLEAR,
    LEFT_PARENTHESIS,
    RIGHT_PARENTHESIS,
    NOT_OPERATION
}
