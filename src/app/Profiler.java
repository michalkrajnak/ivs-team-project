/*******************************************************************
 * Project name: IVS - Calculator
 * File: Profiler.java
 * Date: 28.4.2017
 * Last modification: 29.4.2017
 * Author: Michal Krajnak xkrajn01(at)stud.fit.vutbr.cz
 *
 * Description: Class for counting Standard Deviation for Profiling.
 *
 *******************************************************************/
/**
 * @file Profiler.java
 *
 * @brief Class for counting Standard Deviation for Profiling.
 * @author Michal Krajnak
 */

package app;

import app.math_lib.MathLib;

public class Profiler {

    public Profiler(Float[] args) {
        System.out.println(standardDeviation(args));
    }

    /**
     *
     * @param args Array containing numbers for counting
     * @return Double
     */
    private Double standardDeviation(Float[] args) {
        Double arithmeticalMean = getArithmeticalMean(args);
        Double sum = getSum(args);

        // (1/(N-1) * (sum - N*arithmeticalMean^2))^1/2
        return MathLib.root(MathLib.mul(MathLib.div(1, MathLib.sub(args.length, 1)), MathLib.sub(sum, MathLib.mul(args.length, MathLib.power(arithmeticalMean, 2)))), 2);
    }

    /**
     *
     * @param args Array containing numbers for counting
     * @return Double
     */
    private Double getSum(Float[] args) {
        Double sum = 0.0;
        for (int i = 0; i < args.length; i++) {
            // counting sum for standard deviation
            // x^2
            sum = MathLib.add(sum, MathLib.power(args[i], 2));
        }
        return sum;
    }

    /**
     *
     * @param args Array containing numbers for counting
     * @return Double
     */
    private Double getArithmeticalMean(Float[] args) {
        Double sum = 0.0;
        for (int i = 0; i < args.length; i++) {
            // counts the sum of all numbers given
            sum = MathLib.add(sum, args[i]);
        }
        // 1/N * sum
        return MathLib.mul(MathLib.div(1, args.length), sum);
    }
}
