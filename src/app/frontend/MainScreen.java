/*******************************************************************
 * Project name: IVS - Calculator
 * File: MainScreen.java
 * Date: 27.4.2017
 * Last modification: 29.4.2017
 * Author: Tomas Hreha xhreha00(at)stud.fit.vutbr.cz
 *
 * Description: Main window.
 *
 *******************************************************************/
/**
 * @file MainScreen.java
 *
 * @brief View class for MainScreen window.
 * @author Tomas Hreha
 */

package app.frontend;

import app.Interlayer;
import app.frontend.jpanel_button.JPanelButton;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class MainScreen {
    public JPanel mainPanel;
    private JPanel buttonsPanel;
    private JLabel errorText;
    private JTextArea textArea1;
    private JLabel result;

    private List<JPanelButton> buttonsValueList = new ArrayList<>();
    private List<JPanelButton> buttonsOperationList = new ArrayList<>();

    private String inputExpression = "";

    private Interlayer interlayer;

    /**
     * Constructor.
     * @param interlayer Object for communication between frontend and backend layers.
     */
    public MainScreen(Interlayer interlayer) {
       this.interlayer = interlayer;

       interlayer.setInputArea(textArea1);
       interlayer.setResultArea(result);
       interlayer.setErrorArea(errorText);

       initButtons();
    }

    /**
     * Method for dynamic binding of view model with button handler class.
     */
    private void initButtons() {
        textArea1.setEditable(true);
        for (Component component: buttonsPanel.getComponents()) {
            if(component instanceof JPanel) {
                secondLevelOfJPanels(component);
            }
        }
    }

    private void secondLevelOfJPanels(Component parentJPanel) {
        for(Component component: ((JPanel) parentJPanel).getComponents()) {
            if(component instanceof JPanel) {
                thirdLevelOfJPanels(component);
            }
        }
    }

    /**
     * Method get value/operation button and parent element object (click object).
     * @param parentComponent Parent component.
     */
    private void thirdLevelOfJPanels(Component parentComponent) {
        String numberPattern = "^[0-9]{1}$";
        Component textComponent = ((JPanel) parentComponent).getComponent(0);
        if(textComponent instanceof JLabel) {
            String buttonText = ((JLabel) textComponent).getText();
            createOperationButton((JPanel) parentComponent, buttonText);
        }
    }

    /**
     * Method create button click handler and bind it to view element
     * @param button
     * @param buttonText
     */
    private void createOperationButton(JPanel button, String buttonText) {
        try {
            buttonsOperationList.add(new JPanelButton(button, buttonText, interlayer));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
}
