/*******************************************************************
 * Project name: IVS - Calculator
 * File: JPanelButton.java
 * Date: 27.4.2017
 * Last modification: 29.4.2017
 * Author: Tomas Hreha xhreha00(at)stud.fit.vutbr.cz
 *
 * Description: Handler for buttons.
 *
 *******************************************************************/
/**
 * @file JPanelButton.java
 *
 * @brief View class for calculator buttons.
 * @author Tomas Hreha
 */

package app.frontend.jpanel_button;

import app.enumerator.Operation;
import app.Interlayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class JPanelButton {

    private JPanel buttonUI;
    private String buttonValue;

    private Color defaultBackgroundColor;
    private static final Color clickedBackgroundColor = Color.decode("#4a148c");

    private Operation operation;
    private Interlayer interlayer;

    /**
     * Constructor.
     * @param jPanel View element for binding.
     * @param value Value of button (number or operation)
     * @param interlayer Layer for communication between frontend and backend.
     */
    public JPanelButton(JPanel jPanel, String value, Interlayer interlayer) {
        this.buttonUI = jPanel;
        this.buttonValue = value;
        this.interlayer = interlayer;

        operation = getOperation(value);

        commonInit();
    }

    /**
     * Set common functionality for both values and operators buttons.
     * Color change on click ...
     */
    private void commonInit() {
        buttonUI.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                clickEvent();
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                if(defaultBackgroundColor == null)
                    defaultBackgroundColor = buttonUI.getBackground();

                buttonUI.setBackground(clickedBackgroundColor);
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                buttonUI.setBackground(defaultBackgroundColor);
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });
    }

    /**
     * Calling inter layer methods for proper respond.
     */
    private void clickEvent() {
        switch (operation) {
            case RESULT:
                interlayer.getResult();
                break;
            case DELETE:
                interlayer.removeLastCharInInput();
                break;
            case CLEAR:
                interlayer.clearScreen();
                break;
            default:
                setText();
                break;
        }
    }

    /**
     * Input clicked symbol into screen.
     */
    private void setText() {
        interlayer.setInputAreaText(buttonValue);
    }

    /**
     * Translate button text to operation enum.
     * @param buttonText Label of button.
     * @return Enum of buttons operation.
     */
    private Operation getOperation(String buttonText) {
        switch (buttonText) {
            case "+":
                return Operation.ADD;
            case "-":
                return Operation.MINUS;
            case "*":
                return Operation.MUL;
            case "/":
                return Operation.DIV;
            case "=":
                return Operation.RESULT;
            case "!":
                return Operation.FAKT;
            case "x^y":
                return Operation.SQR;
            case "sqrt":
                return Operation.SQRT;
            case "?":
                return Operation.CUSTOM;
            case "c":
                return Operation.DELETE;
            case "clear":
                return Operation.CLEAR;
            case "(":
                return Operation.LEFT_PARENTHESIS;
            case ")":
                return Operation.RIGHT_PARENTHESIS;
            default:
                return Operation.NOT_OPERATION;
        }
    }
}
