/*******************************************************************
* Project name: IVS - Calculator
* File: MathLib.java
* Date: 25.4.2017
* Last modification: 26.4.2017
* Author: Dušan Valecký xvalec00(at)stud.fit.vutbr.cz
*
* Description: Math library with basic mathematical operations.
*
*******************************************************************/
/**
* @file MathLib.java
*
* @brief Math library with basic mathematical operations.
* @author Dušan Valecký
*/

package app.math_lib;

import app.backend.exception.WrongExpressionException;
import java.lang.Math;

public class MathLib {
	/** Enumeration type for errors detection. */
	public static enum errors {
		MATH_NO_ERROR,
		MATH_ERROR_DIV_BY_ZERO,
		MATH_INVALID_INPUT
	}
	
	/** Indicates errors */
	public static errors Err;
	
	/**
	 * Returns the sum of its arguments.
	 * @param a The first value.
	 * @param b The second value.
	 * @return The result.
	 */
	public static double add(double a, double b){
		return a + b;
	}
	
	/**
	 * Returns the difference of the arguments.
	 * @param a The first value.
	 * @param b The second value to subtract from the first.
	 * @return The result.
	 */
	public static double sub(double a, double b){
		return a - b;
	}
	
	/**
	 * Returns first value divided by second value.
	 * @param a The dividend.
	 * @param b The divisor.
	 * @return First value divided by second value. If b == 0, sets error and returns 0.
	 * @throws WrongExpressionException If division by zero
	 */
	public static double div(double a, double b) {
		if (b == 0.0) {
			throw new WrongExpressionException("Division by zero.");
		}
		return a / b;
	}

	
	/**
	 * Returns the multiple of two double values.
	 * @param a The first value.
	 * @param b The second value.
	 * @return Multiplied values.
	 */
	public static double mul(double a, double b){
		return a * b;
	}
	
	/**
	 * Returns the factorial of given value.
	 * @param x Given value.
	 * @return Factorial x (x!). If x < 1, sets error and returns 0.
	 * @throws WrongExpressionException If number lower than 1.
	 */
	public static double factorial(double x){
		if(x < 1){
			throw new WrongExpressionException("The number must be bigger than or equal to 1.");
		} else  if(x == 1) {
			return 1.0;
		} else {
			return x * factorial(x - 1);
		}
	}
	
	/**
	 * Returns the value of the first argument raised to the power of the second argument.
	 * @param x The base.
	 * @param a The exponent.
	 * @return Value x^a.
	 * @throws WrongExpressionException If exponent lower than 0.
	 */
	public static double power(double x, double a){
		if(x == 0) {
			if(a > 0) { // if a > 0, 0^a = 0
				return 0.0;
			}
			else if(a < 0) { // if a < 0, 0^a = NaN
				throw new WrongExpressionException("Exponent must be greater than or equal to 0.");
			}
			else { // 0^0 = 1
				return 1.0;
			}
		}
        
        return Math.pow(x, a);
	}
	
	/**
	 * Returns a-th root of a double value.
	 * @see Math.pow(double x, double a)
	 * @param x X value.
	 * @param a A-th root.
	 * @return A-th root of x.
	 * @throws WrongExpressionException If A-th root is lower than 0
	 */
	public static double root(double x, double a){
		if(x < 0) {
			throw new WrongExpressionException("A-Th root must be greater than or equal to 0.");
		}
		else {
			return Math.pow(x, 1 / a);
		}
	}
	
	/**
	 * Returns Euler's number e raised to the power of a double value.
	 * @see Math.exp(double a)
	 * @param a The exponent.
	 * @return Value e^x.
	 */
	public static double exp(double a){
		return Math.exp(a);
	}
}
/*** End of file MathLib.java ***/
