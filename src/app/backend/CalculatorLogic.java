/*******************************************************************
 * Project name: IVS - Calculator
 * File: CalculatorLogic.java
 * Date: 27.4.2017
 * Last modification: 29.4.2017
 * Author: Tomas Hreha xhreha00(at)stud.fit.vutbr.cz
 *
 * Description: Class for calculating input expression.
 *
 *******************************************************************/
/**
 * @file CalculatorLogic.java
 *
 * @brief Creating precedence processing class an RPN class for calculating.
 * @author Tomas Hreha
 */

package app.backend;

import app.backend.exception.WrongExpressionException;
import app.backend.precedence.PrecedenceOperation;
import app.backend.precedence.PrecedenceProcessing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static app.backend.precedence.PrecedenceOperation.*;

public class CalculatorLogic {

    private ReversePolishNotation reversePolishNotation;
    private PrecedenceProcessing precedenceProcessing;

    public CalculatorLogic() {
        reversePolishNotation = new ReversePolishNotation();
        precedenceProcessing = new PrecedenceProcessing();
    }

    /**
     * Main class method. Receive input string - forward it to precedence processing.
     * Result of precedence processing forward to RPN processing.
     * Receive result in string from RPN, forward it to frontend for displaying to user.
     * @param expression Input math expression for calculating.
     * @return Result of expression.
     * @throws WrongExpressionException In case of invalid expression.
     */
    public String calculate(String expression) throws WrongExpressionException {

        expression = transformSqrt(expression);

        List<String> precedenceResult = precedenceProcessing.precedenceProcessing(expression);

        String result = "";
        try {
            result = reversePolishNotation.count(precedenceResult);
        } catch (WrongExpressionException e) {
            e.printStackTrace();
            throw e;
        } finally {
            precedenceProcessing.cleanUp();
        }

        return result;
    }

    /**
     * Calculator logic demand sqrt write as s operator.
     * This method transform sqrt string to s character.
     * @param expression Input expression.
     * @return New expression for processing.
     * @throws WrongExpressionException In case of wrong sqrt spelling.
     */
    private String transformSqrt(String expression) {
        String resultString = "";
        for(int i = 0; i < expression.length(); i++) {
            if(expression.charAt(i) == 's') {
                if(i + 4 > expression.length())
                    throw new WrongExpressionException("Wrong expression");
                if(expression.substring(i, i + 4).equals("sqrt")) {
                    resultString += 's';
                    i += 3;
                } else {
                    throw new WrongExpressionException("Wrong expression!");
                }
            } else {
                resultString += expression.charAt(i);
            }
        }
        return resultString;
    }
}
