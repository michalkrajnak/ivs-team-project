package app.backend.exception;

public class WrongExpressionException extends RuntimeException {

    public WrongExpressionException(String message) {
        super(message);
    }
}
