/*******************************************************************
 * Project name: IVS - Calculator
 * File: ReversePolishNotation.java
 * Date: 27.4.2017
 * Last modification: 28.4.2017
 * Author: Michal Krajnak xkrajn01(at)stud.fit.vutbr.cz
 *
 * Description: Class for counting math expression using Reverse Polish Notation.
 *
 *******************************************************************/
/**
 * @file ReversePolishNotation.java
 *
 * @brief Class for counting math expression using Reverse Polish Notation.
 * @author Michal Krajnak
 */

package app.backend;

import app.backend.exception.WrongExpressionException;
import app.math_lib.MathLib;

import java.util.ArrayList;
import java.util.List;

public class ReversePolishNotation {

    public ReversePolishNotation() {

    }

    /**
     *
     * @param expression List of elements created by ShuntingYard Algorithm
     * @return double
     * @throws WrongExpressionException Throws exception if operator is invalid
     */
    public String count(List<String> expression) throws WrongExpressionException {

        List<String> stack = new ArrayList<>();

        for (int i = 0; i < expression.size(); i++) {
            String value = expression.get(i);
            if(isNumeric(value)) {
                stack.add(value);
            } else {
                double firstOperand, secondOperand;
                int position;
                switch (value) {
                    case "+":
                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        position = stack.size()-1;
                        secondOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.add(firstOperand, secondOperand)));
                        break;
                    case "-":
                        position = stack.size()-1;
                        secondOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.sub(firstOperand, secondOperand)));
                        break;
                    case "*":
                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        position = stack.size()-1;
                        secondOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.mul(firstOperand, secondOperand)));
                        break;
                    case "/":
                        position = stack.size()-1;
                        secondOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.div(firstOperand, secondOperand)));
                        break;
                    case "s":

                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.root(firstOperand, 2)));
                        break;
                    case "!":
                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.factorial(firstOperand)));
                        break;
                    case "e":
                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.exp(firstOperand)));
                        break;
                    case "^":
                        position = stack.size()-1;
                        secondOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        position = stack.size()-1;
                        firstOperand = Double.parseDouble(stack.get(position));
                        stack.remove(position);
                        stack.add(String.valueOf(MathLib.power(firstOperand, secondOperand)));
                        break;
                    default:
                        throw new WrongExpressionException("Wrong character.");
                }
            }
        }
        return stack.get(0);
    }

    /**
     *
     * @param str String to validate if it is numeric
     * @return bool
     */
    private static boolean isNumeric(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
}