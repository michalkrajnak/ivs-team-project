/*******************************************************************
 * Project name: IVS - Calculator
 * File: MathLib.java
 * Date: 27.4.2017
 * Last modification: 29.4.2017
 * Author: Tomas Hreha xhreha00(at)stud.fit.vutbr.cz
 *
 * Description: Enumerator for operation in precedence processing.
 *
 *******************************************************************/
/**
 * @file PrecedenceOperation.java
 *
 * @brief Enumarator for operation in precedence processing
 * @author Tomas Hreha
 */

package app.backend.precedence;

public enum PrecedenceOperation {
    OP_ADD,
    OP_MINUS,
    OP_MUL,
    OP_DIV,
    OP_SQR,
    OP_SQRT,
    OP_FAK,
    OP_CUST,
    LEFT_PARENTHESIS,
    RIGHT_PARENTHESIS,
    EXP,
    STACK_PUSH,
    STACK_POP,
    OP_END,
    OP_START,
    EMPTY;
}

