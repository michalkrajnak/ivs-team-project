/*******************************************************************
 * Project name: IVS - Calculator
 * File: PrecedenceProcessing.java
 * Date: 27.4.2017
 * Last modification: 29.4.2017
 * Author: Tomas Hreha xhreha00(at)stud.fit.vutbr.cz
 *
 * Description: Class for precedence processing.
 *
 *******************************************************************/
/**
 * @file PrecedenceProcessing.java
 *
 * @brief Get input string - create list of RPN tokens list.
 * @author Tomas Hreha
 */

package app.backend.precedence;

import app.backend.exception.WrongExpressionException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static app.backend.precedence.PrecedenceOperation.*;

public class PrecedenceProcessing {

    private List<String> operandsStack = new ArrayList<>();
    private List<String> outputStack = new ArrayList<>();

    private Map<String, PrecedenceOperation> precedenceRules = new HashMap<>();
    private List<String> precedenceValidExp = new ArrayList<>();

    /**
     * Constructor.
     * Initialized rules for shunting yard algorithm and rules for valid input.
     */
    public PrecedenceProcessing() {
        initPrecedenceRules();
        initPrecedenceValidExp();
    }

    /**
     * Start precedence processing - receive input string, return list of RPN tokens.
     * @param expression Input string (math expression).
     * @return List of RPN tokens for further processing.
     */
    public List<String> precedenceProcessing(String expression) {

        checkValidity("#" + expression + "$");
        shuntingYardProcessing(expression + "$");

        return outputStack;
    }

    /**
     * Check validity of input string.
     * Based on predefined rules throws exception if input string is invalid math expression.
     * @param expression Input string.
     * @throws WrongExpressionException If string is not valid.
     */
    private void checkValidity(String expression) throws WrongExpressionException {
        for (int i = 0; i < expression.length() - 1; i++) {
            if(!precedenceValidExp.contains(getHashKey(getOpFromString(expression.charAt(i)), getOpFromString(expression.charAt(i + 1))))) {
                throw new WrongExpressionException("Wrong expression!");
            }
        }
    }

    /**
     * Main method for shunting yard processing.
     * Get input string, create output stack of RPN tokens.
     * @param expression Input string.
     */
    private void shuntingYardProcessing(String expression) {
        int i = copyExpToOutput(expression, 0);
        for (; i < expression.length(); i++) {
            switch (getOpFromString(expression.charAt(i))) {
                case EXP:
                    i = copyExpToOutput(expression, i) - 1;
                    break;
                case OP_FAK:
                    outputStack.add(String.valueOf(expression.charAt(i)));
                    break;
                case LEFT_PARENTHESIS:
                    operandsStack.add(String.valueOf(expression.charAt(i)));
                    i = copyExpToOutput(expression, i) - 1;
                    break;
                case RIGHT_PARENTHESIS:
                    copyToOutput(operandsStack.get(operandsStack.size() - 1));
                    break;
                case OP_CUST:
                    operandInputProcess(String.valueOf(expression.charAt(i)));
                    i += 1;
                    break;
                default:
                    operandInputProcess(String.valueOf(expression.charAt(i)));
                    break;
            }
        }
    }

    /**
     * Method for handling more than one cipher numbers and negative numbers.
     * @param expression Input string.
     * @param i Position where is currently main processing loop.
     * @return New position in input string for main loop.
     */
    private int copyExpToOutput(String expression, int i) {
        String num = "";
        for(; i < expression.length(); i++) {
            if(getOpFromString(expression.charAt(i)) == EXP || expression.charAt(i) == '.') {
                num += expression.charAt(i);
            } else {
                if(i == 0 && getOpFromString(expression.charAt(i)) == OP_MINUS) {
                    num += expression.charAt(i);
                } else if(getOpFromString(expression.charAt(i)) == LEFT_PARENTHESIS) {
                    if((getOpFromString(expression.charAt(i + 1)) == OP_MINUS)) {
                        i += 1;
                        num += expression.charAt(i);
                    }
                } else {
                    break;
                }
            }
        }
        if(!num.equals("") && !num.isEmpty())
            outputStack.add(num);
        return i;
    }

    /**
     * Method for handling parenthesis. Loop through operators stack until opening parenthesis.
     * @param inputOp Operator last loaded from input string.
     */
    private void copyToOutput(String inputOp) {
        if(inputOp.equals("(")) {
            operandsStack.remove(operandsStack.size() - 1);
        } else {
            outputStack.add(operandsStack.get(operandsStack.size() - 1));
            operandsStack.remove(operandsStack.size() - 1);
            if (operandsStack.size() != 0)
                copyToOutput(operandsStack.get(operandsStack.size() - 1));
        }
    }

    /**
     * Method for operator processing.
     * Process last loaded operator based on precedence rules.
     * @param inputOp Last loaded operator from input expression.
     */
    private void operandInputProcess(String inputOp) {
        if(getShuntingYardOperation(inputOp) == STACK_PUSH) {
            operandsStack.add(inputOp);
        } else {
            outputStack.add(operandsStack.get(operandsStack.size() - 1));
            operandsStack.remove(operandsStack.size() - 1);
            operandInputProcess(inputOp);
        }
    }

    /**
     * Method returns operation POP or PUSH based on precedence rules.
     * @param expression Input token, which is compared with character on top of operators stack.
     * @return Precedence rule PUSH or POP.
     * @throws WrongExpressionException In case of wrong pair of operations.
     */
    private PrecedenceOperation getShuntingYardOperation(String expression) throws WrongExpressionException {
        if(operandsStack.isEmpty())
            return STACK_PUSH;
        String inputHashKey = String.valueOf(getOpFromString(expression.charAt(0)).hashCode());
        String topStackHashKey = String.valueOf(getOpFromString(operandsStack.get(operandsStack.size() - 1).charAt(0)).hashCode());
        return precedenceRules.get(inputHashKey + topStackHashKey);
    }

    /**
     * Convert char to operation enum.
     * @param stringOp Char format of input token.
     * @return Enum value of input char.
     */
    private PrecedenceOperation getOpFromString(char stringOp) {
        switch (stringOp) {
            case '+':
                return OP_ADD;
            case '-':
                return OP_MINUS;
            case '*':
                return OP_MUL;
            case '/':
                return OP_DIV;
            case '^':
                return OP_SQR;
            case 's': // s for sqrt
                return OP_SQRT;
            case '!':
                return OP_FAK;
            case 'e':
                return OP_CUST;
            case '(':
                return LEFT_PARENTHESIS;
            case ')':
                return RIGHT_PARENTHESIS;
            case '#':
                return OP_START;
            case '$':
                return OP_END;
            default:
                return EXP;
        }
    }

    /**
     * Initialization of precedence rules.
     * For pair of operation is created hashcode, which is unique key in list of rules.
     * Based on this hashcode is picked correct precedence rule.
     */
    private void initPrecedenceRules() {
        // Rules for +-*/^sqrt as input and + as operation on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, OP_ADD), STACK_POP);
        precedenceRules.put(getHashKey(OP_MINUS, OP_ADD), STACK_POP);
        precedenceRules.put(getHashKey(OP_MUL, OP_ADD), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_DIV, OP_ADD), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQR, OP_ADD), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQRT, OP_ADD), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_END, OP_ADD), STACK_POP);
        // Rules for +-*/^sqrt as input and - as operation on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, OP_MINUS), STACK_POP);
        precedenceRules.put(getHashKey(OP_MINUS, OP_MINUS), STACK_POP);
        precedenceRules.put(getHashKey(OP_MUL, OP_MINUS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_DIV, OP_MINUS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQR, OP_MINUS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQRT, OP_MINUS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_END, OP_MINUS), STACK_POP);
        // Rules for +-*/^sqrt as input and * as operations on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, OP_MUL), STACK_POP);
        precedenceRules.put(getHashKey(OP_MINUS, OP_MUL), STACK_POP);
        precedenceRules.put(getHashKey(OP_MUL, OP_MUL), STACK_POP);
        precedenceRules.put(getHashKey(OP_DIV, OP_MUL), STACK_POP);
        precedenceRules.put(getHashKey(OP_SQR, OP_MUL), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQRT, OP_MUL), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_END, OP_MUL), STACK_POP);
        // Rules for +-*/^sqrt as input and / as operations on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, OP_DIV), STACK_POP);
        precedenceRules.put(getHashKey(OP_MINUS, OP_DIV), STACK_POP);
        precedenceRules.put(getHashKey(OP_MUL, OP_DIV), STACK_POP);
        precedenceRules.put(getHashKey(OP_DIV, OP_DIV), STACK_POP);
        precedenceRules.put(getHashKey(OP_SQR, OP_DIV), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQRT, OP_DIV), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_END, OP_DIV), STACK_POP);
        // Rules for +-*/^sqrt as input and ^ as operations on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, OP_SQR), STACK_POP);
        precedenceRules.put(getHashKey(OP_MINUS, OP_SQR), STACK_POP);
        precedenceRules.put(getHashKey(OP_MUL, OP_SQR), STACK_POP);
        precedenceRules.put(getHashKey(OP_DIV, OP_SQR), STACK_POP);
        precedenceRules.put(getHashKey(OP_SQR, OP_SQR), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQRT, OP_SQR), STACK_POP);
        precedenceRules.put(getHashKey(OP_END, OP_SQR), STACK_POP);
        // Rules for +-*/^sqrt as input and sqrt as operations on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, OP_SQRT), STACK_POP);
        precedenceRules.put(getHashKey(OP_MINUS, OP_SQRT), STACK_POP);
        precedenceRules.put(getHashKey(OP_MUL, OP_SQRT), STACK_POP);
        precedenceRules.put(getHashKey(OP_DIV, OP_SQRT), STACK_POP);
        precedenceRules.put(getHashKey(OP_SQR, OP_SQRT), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQRT, OP_SQRT), STACK_POP);
        precedenceRules.put(getHashKey(OP_END, OP_SQRT), STACK_POP);
        // Rules for +-*/^sqrt as input and sqrt as operations on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, OP_CUST), STACK_POP);
        precedenceRules.put(getHashKey(OP_MINUS, OP_CUST), STACK_POP);
        precedenceRules.put(getHashKey(OP_MUL, OP_CUST), STACK_POP);
        precedenceRules.put(getHashKey(OP_DIV, OP_CUST), STACK_POP);
        precedenceRules.put(getHashKey(OP_SQR, OP_CUST), STACK_POP);
        precedenceRules.put(getHashKey(OP_SQRT, OP_CUST), STACK_POP);
        precedenceRules.put(getHashKey(OP_END, OP_CUST), STACK_POP);
        // Rules for +-*/^sqrt as input and ( as operations on top of operation stack
        precedenceRules.put(getHashKey(OP_ADD, LEFT_PARENTHESIS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_MINUS, LEFT_PARENTHESIS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_MUL, LEFT_PARENTHESIS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_DIV, LEFT_PARENTHESIS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQR, LEFT_PARENTHESIS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_SQRT, LEFT_PARENTHESIS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_CUST, LEFT_PARENTHESIS), STACK_PUSH);
        precedenceRules.put(getHashKey(OP_END, LEFT_PARENTHESIS), STACK_PUSH);
    }

    /**
     * Initialization of valid pairs in input expression.
     * Pair consist of 2 tokens from input string.
     * Using unique hashcode is later checked validity of input expression.
     */
    private void initPrecedenceValidExp() {
        // Rules for +-*/ as input and ( as operation on top of operation stack
        List<PrecedenceOperation> operands = new ArrayList<>();
        operands.add(OP_ADD);
        operands.add(OP_MINUS);
        operands.add(OP_MUL);
        operands.add(OP_DIV);
        operands.add(OP_SQR);

        // 5 + || 5 - ...
        for(int i = 0; i < operands.size(); i++) {
            precedenceValidExp.add(getHashKey(EXP, operands.get(i)));
        }
        precedenceValidExp.add(getHashKey(EXP, OP_FAK));
        // + 5 || - 5 ...
        for(int i = 0; i < operands.size(); i++) {
            precedenceValidExp.add(getHashKey(operands.get(i), EXP));
        }
        // + sqrt || - sqrt ...
        for(int i = 0; i < operands.size(); i++) {
            precedenceValidExp.add(getHashKey(operands.get(i), OP_SQRT));
        }
        // + ( || - ( ...
        for(int i = 0; i < operands.size(); i++) {
            precedenceValidExp.add(getHashKey(operands.get(i), LEFT_PARENTHESIS));
        }
        // + ( || - ( ...
        for(int i = 0; i < operands.size(); i++) {
            precedenceValidExp.add(getHashKey(RIGHT_PARENTHESIS, operands.get(i)));
        }
        // ! + || ! - ...
        for(int i = 0; i < operands.size(); i++) {
            precedenceValidExp.add(getHashKey(OP_FAK, operands.get(i)));
        }
        // )!
        precedenceValidExp.add(getHashKey(RIGHT_PARENTHESIS, OP_FAK));
        // sqrt(
        precedenceValidExp.add(getHashKey(OP_SQRT, LEFT_PARENTHESIS));
        // sqrt5
        precedenceValidExp.add(getHashKey(OP_SQRT, EXP));

        precedenceValidExp.add(getHashKey(RIGHT_PARENTHESIS, OP_END));
        precedenceValidExp.add(getHashKey(EXP, OP_END));
        precedenceValidExp.add(getHashKey(OP_FAK, OP_END));

        precedenceValidExp.add(getHashKey(OP_START, EXP));
        precedenceValidExp.add(getHashKey(OP_START, LEFT_PARENTHESIS));
        precedenceValidExp.add(getHashKey(OP_START, OP_SQRT));

        //(sqrt
        precedenceValidExp.add(getHashKey(LEFT_PARENTHESIS, OP_SQRT));
        // (5
        precedenceValidExp.add(getHashKey(LEFT_PARENTHESIS, EXP));
        // ((
        precedenceValidExp.add(getHashKey(LEFT_PARENTHESIS, LEFT_PARENTHESIS));


        // 5)
        precedenceValidExp.add(getHashKey(EXP, RIGHT_PARENTHESIS));
        // !(
        precedenceValidExp.add(getHashKey(OP_FAK, RIGHT_PARENTHESIS));
        // ))
        precedenceValidExp.add(getHashKey(RIGHT_PARENTHESIS, RIGHT_PARENTHESIS));
        // 55 / 5. / .5
        precedenceValidExp.add(getHashKey(EXP, EXP));
        // -5
        precedenceValidExp.add(getHashKey(OP_START, OP_MINUS));
        // -5
        precedenceValidExp.add(getHashKey(LEFT_PARENTHESIS, OP_MINUS));

        // custom e^x
        precedenceValidExp.add(getHashKey(OP_CUST, OP_SQR));
        // + e || - e ...
        for(int i = 0; i < operands.size(); i++) {
            precedenceValidExp.add(getHashKey(operands.get(i), OP_CUST));
        }
        // (e
        precedenceValidExp.add(getHashKey(LEFT_PARENTHESIS, OP_CUST));
        // sqrte
        precedenceValidExp.add(getHashKey(OP_SQRT, OP_CUST));
        // e
        precedenceValidExp.add(getHashKey(OP_START, OP_CUST));
    }

    /**
     * Get two precedence enums and return their concatenate hashcodes.
     * @param firstOp First operation.
     * @param secondOp Second operation.
     * @return String value of concatenated hascodes of input values.
     */
    private String getHashKey(PrecedenceOperation firstOp, PrecedenceOperation secondOp) {
        return String.valueOf(firstOp.hashCode()) + String.valueOf(secondOp.hashCode());
    }

    public void cleanUp() {
        outputStack.removeAll(outputStack);
        operandsStack.removeAll(operandsStack);
    }
}
