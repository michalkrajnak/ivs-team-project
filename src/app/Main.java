/*******************************************************************
 * Project name: IVS - Calculator
 * File: JPanelButton.java
 * Date: 18.3.2017
 * Last modification: 29.4.2017
 * Author: Tomas Hreha xhreha00(at)stud.fit.vutbr.cz
 *
 * Description: Main app class.
 *
 *******************************************************************/
/**
 * @file Main.java
 *
 * @brief Class creates window and lunch app.
 * @author Tomas Hreha
 */
package app;
/**
 * app.Main class of application.
 * Initiate application app.frontend and app.backend.
 */

import javax.swing.*;
import java.awt.*;
import java.util.Random;

import app.backend.CalculatorLogic;
import app.backend.ReversePolishNotation;
import app.frontend.*;

/**
 * Create main frame and lunch main app.backend function.
 *
 * @author Tomas Hreha
 */
public class Main {

    public static final JFrame mainFrame = new JFrame();

    /**
     * app.Main constructor.
     * Create main window and lunch app.backend main function for correct app functionality.
     */
    public static void main(String[] args) {

        Interlayer interlayer = new Interlayer(new CalculatorLogic());

        MainScreen mainScreen = new MainScreen(interlayer);

        mainFrame.setSize(850, 850);
        mainFrame.setContentPane(mainScreen.mainPanel);
        mainFrame.setMinimumSize(new Dimension(500, 500));

        mainFrame.setVisible(true);
    }
}
