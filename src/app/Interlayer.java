package app;

import app.backend.CalculatorLogic;
import app.backend.exception.WrongExpressionException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static javax.swing.JComponent.WHEN_FOCUSED;

/**
 * Created by tomas on 4/25/17.
 */
public class Interlayer {

    private JTextArea inputArea;
    private JLabel resultArea;
    private JLabel errorArea;

    private boolean isResultHit = false;
    private boolean isEnterHit;

    private CalculatorLogic calculatorLogic;

    public Interlayer(CalculatorLogic calculatorLogic) {
        this.calculatorLogic = calculatorLogic;
    }

    public JTextArea getInputArea() {
        return inputArea;
    }

    public void setInputAreaText(String inputText) {
        if(isResultHit) {
            clearScreen();
            isResultHit = false;
        } else {
            isEnterHit = true;
        }

        int caretPosition = getCaretPosition();
        String start = getInputAreaText().substring(0, caretPosition);
        String end = getInputAreaText().substring(caretPosition, getInputAreaText().length());
        this.inputArea.setText(start + inputText + end);
        setCaretPosition(start.length() + inputText.length());
    }

    public JLabel getResultArea() {
        return resultArea;
    }

    public void setResultText(String resultText) {
        this.resultArea.setText(resultText);
        errorArea.setText("");
    }

    public JLabel getErrorArea() {
        return errorArea;
    }

    public void setErrorText(String errorText) {
        this.errorArea.setText(errorText);
        resultArea.setText("");
    }

    public String getInputAreaText() {
        return inputArea.getText();
    }

    public int getCaretPosition() {
        return inputArea.getCaretPosition();
    }

    public void setCaretPosition(int position) {
        inputArea.setCaretPosition(position);
    }

    public void removeLastCharInInput() {
        int caretPosition = getCaretPosition();
        if(caretPosition == 0)
            return;

        String start = getInputAreaText().substring(0, caretPosition - 1);
        String end = getInputAreaText().substring(caretPosition, getInputAreaText().length());

        inputArea.setText("");
        this.inputArea.setText(start + end);
        setCaretPosition(start.length());

//        String tmp = getInputAreaText().substring(0, getInputAreaText().length() - 1);
//
//        setInputAreaText(tmp);
    }

    public void getResult() {
        isResultHit = true;
        String result = "";
        try {
            result = calculatorLogic.calculate(getInputAreaText());
            setResultText(result);
        } catch (WrongExpressionException e) {
            e.printStackTrace();
            setErrorText(e.getMessage());
        }
    }

    public void clearScreen() {
        isEnterHit = true;
        isResultHit = false;
        resultArea.setText("");
        inputArea.setText("");
        errorArea.setText("");
    }

    public void setInputArea(JTextArea inputArea) {
        this.inputArea = inputArea;
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                getResult();
                isEnterHit = true;
            }
        };
        inputArea.registerKeyboardAction(actionListener, "command", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), WHEN_FOCUSED);
        inputArea.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                /* TODO: Tomas 25.4.2017 Not work properly when user change input from keyboard with input from window */
                if(isEnterHit) {
                    isEnterHit = false;
                } else {
                    if(!getResultArea().getText().equals("") && !getResultArea().getText().isEmpty())
                        clearScreen();
                    else
                        isEnterHit = true;
                }
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });
        this.inputArea = inputArea;
    }

    public void setResultArea(JLabel resultArea) {
        this.resultArea = resultArea;
    }

    public void setErrorArea(JLabel errorArea) {
        this.errorArea = errorArea;
    }
}
